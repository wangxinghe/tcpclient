package au.com.philology.tcpclient;

import au.com.philology.common.JavaTheme;
import javax.swing.SwingUtilities;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ricolwang
 */
public class ClientDemo
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        JavaTheme.setLookAndFeel(JavaTheme.LOOKANDFEEL_MOTIF);
        SwingUtilities.invokeLater(new Runnable()
        {
            public void run()
            {
                new FrameMain().setVisible(true);
            }
        });
    }

}
